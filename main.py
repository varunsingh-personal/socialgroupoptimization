from __future__ import annotations

import math
import random
from typing import List

import matplotlib.pyplot as plot


class Person:
    # Initialize Person
    def __init__(self, traits: List[float]):
        self.fitness = None
        self.traits = traits
        self.retrospective_coefficient = 0.2

    # Calculate fitness of a Person using fitness_function (SphereFunction)
    def calculate_fitness(self):
        self.fitness = SphereFunction.run(self.traits)

    # Improve traits from other_person
    def improve_traits_from(self, other_person: Person):
        new_traits = []
        for i in range(0, len(self.traits)):
            old_trait = self.traits[i]

            new_trait = self.retrospective_coefficient * old_trait + random.random() * (
                    other_person.traits[i] - old_trait)
            new_traits.append(new_trait)
        new_fitness = SphereFunction.run(new_traits)
        if SphereFunction.new_fitness_better_than(old_fitness=self.fitness, new_fitness=new_fitness):
            self.traits = new_traits
            self.fitness = new_fitness

    # Acquire traits from other_person and best_person
    def acquire_traits_from(self, other_person: Person, best_person: Person):
        new_traits = []
        for i in range(0, len(self.traits)):
            old_trait = self.traits[i]
            new_trait = old_trait + random.random() * (old_trait - other_person.traits[i]) + random.random() * (
                    best_person.traits[i] - old_trait)
            new_traits.append(new_trait)
        new_fitness = SphereFunction.run(new_traits)
        if SphereFunction.new_fitness_better_than(old_fitness=self.fitness, new_fitness=new_fitness):
            self.traits = new_traits
            self.fitness = new_fitness


# Sphere function
class SphereFunction:

    # Calculates fitness using traits
    @staticmethod
    def run(traits: List[float]) -> float:
        return sum([x * x for x in traits])

    # Check if new fitness is better than existing fitness
    @staticmethod
    def new_fitness_better_than(old_fitness: float, new_fitness: float) -> bool:
        return new_fitness < old_fitness

    # Check if Person has better fitness than other_person
    @staticmethod
    def person_has_better_fitness_than(other_person: Person, to_compare_person: Person) -> bool:
        return to_compare_person.fitness < other_person.fitness

    @staticmethod
    def find_best_person(population: List[Person]):
        return min(population, key=lambda x: x.fitness)


class StepFunction:

    # Calculates fitness using traits
    @staticmethod
    def run(traits: List[float]) -> float:
        return sum([math.floor(x + 0.5) ** 2 for x in traits])

    # Check if new fitness is better than existing fitness
    @staticmethod
    def new_fitness_better_than(old_fitness: float, new_fitness: float) -> bool:
        return new_fitness < old_fitness

    # Check if Person has better fitness than other_person
    @staticmethod
    def person_has_better_fitness_than(other_person: Person, to_compare_person: Person) -> bool:
        return to_compare_person.fitness < other_person.fitness

    @staticmethod
    def find_best_person(population: List[Person]):
        return min(population, key=lambda x: x.fitness)


class PopulationGroup:
    def __init__(self):
        self.population: List[Person] = []
        self.best_person: Person or None = None
        self.best_fitness_history = []

    # Populates based on given params [count, number of traits, range of traits (min, max)]
    def populate(self, person_count: int, traits_count: int, round_off_traits: bool, traits_range_min: float,
                 traits_range_max: float):

        for i in range(0, person_count):
            traits = []
            if round_off_traits:
                for j in range(0, traits_count):
                    value = random.uniform(traits_range_min, traits_range_max)
                    value = math.ceil(value) if math.ceil(value) < traits_range_max else math.floor(value)
                    traits.append(value)
            else:
                for j in range(0, traits_count):
                    value = random.uniform(traits_range_min, traits_range_max)
                    traits.append(value)
            person = Person(traits)
            self.population.append(person)

    # Runs fitness function for each person
    def calculate_population_fitness(self):
        for person in self.population:
            person.calculate_fitness()

    # finds person with the best fitness in population
    def find_best_person(self):
        self.best_person = SphereFunction.find_best_person(self.population)

    # Each person improves traits from other people in population
    def improve_traits(self):
        for person in self.population:
            if person is not self.best_person:
                person.improve_traits_from(self.best_person)

    # Each person acquires knowledge from other people including best person
    def acquire_traits(self):
        for idx in range(0, len(self.population)):
            person = self.population[idx]
            random_person = self.population[random.choice([i for i in range(0, len(self.population)) if i != idx])]
            if SphereFunction.person_has_better_fitness_than(other_person=random_person, to_compare_person=person):
                random_person.acquire_traits_from(person, self.best_person)
            else:
                person.acquire_traits_from(random_person, self.best_person)

    # Runs overall logic based on flow-chart given in SGO Paper
    def run_generations(self, generations: int):
        self.calculate_population_fitness()

        for _ in range(0, generations):
            self.find_best_person()
            self.improve_traits()
            self.find_best_person()
            populationGroup.best_fitness_history.append(populationGroup.best_person.fitness)
            self.acquire_traits()


if __name__ == '__main__':
    generation_value = 10
    populationGroup = PopulationGroup()
    populationGroup.populate(person_count=100, traits_count=50, round_off_traits=True, traits_range_min=-100,
                             traits_range_max=100)
    populationGroup.run_generations(generations=generation_value)
    populationGroup.find_best_person()
    print("Best person's traits")
    for t in populationGroup.best_person.traits:
        print(t, end=" ")
    populationGroup.best_fitness_history.append(populationGroup.best_person.fitness)
    x_axis = range(0, generation_value + 1)
    y_axis = populationGroup.best_fitness_history.copy()
    plot.plot(x_axis, y_axis)
    plot.draw()
    plot.show()
