import unittest

from main import StepFunction, Person, PopulationGroup


class TestMain(unittest.TestCase):

    # Sphere function

    def test_sphere_function_with_same_value(self):
        self.assertEqual(4, StepFunction.run([1, 1, 1, 1]))

    def test_sphere_function_with_different_values(self):
        self.assertEqual(30, StepFunction.run([1, 2, 3, 4]))

    def test_Sphere_Function_has_better_fitness_than_true(self):
        self.assertEqual(True, StepFunction.new_fitness_better_than(2, 1))

    def test_Sphere_Function_has_better_fitness_than_false(self):
        self.assertEqual(False, StepFunction.new_fitness_better_than(1, 2))

    def test_Sphere_Function_person_has_better_fitness_than_true(self):
        compare_with_person = Person(traits=[1, 2, 3, 4])
        compare_with_person.calculate_fitness()
        to_compare_person = Person(traits=[1, 1, 1, 1])
        to_compare_person.calculate_fitness()
        self.assertEqual(True,
                         StepFunction
                         .person_has_better_fitness_than(other_person=compare_with_person,
                                                         to_compare_person=to_compare_person))

    def test_Sphere_Function_person_has_better_fitness_than_false(self):
        compare_with_person = Person(traits=[1, 1, 1, 1])
        compare_with_person.calculate_fitness()
        to_compare_person = Person(traits=[1, 2, 3, 4])
        to_compare_person.calculate_fitness()
        self.assertEqual(False,
                         StepFunction
                         .person_has_better_fitness_than(other_person=compare_with_person,
                                                         to_compare_person=to_compare_person))

    # Person
    def test_person_fitness_with_sphere_function(self):
        person = Person([1, 2, 3, 4])
        person.calculate_fitness()
        self.assertEqual(30, person.fitness)

    # def test_person_should_not_improve_traits_from_lower_fitness(self):
    #     person = Person(traits=[1, 1, 1, 1], fitness_function=SphereFunction)
    #     person.calculate_fitness()
    #     other_person = Person(traits=[1, 2, 3, 4], fitness_function=SphereFunction)
    #     other_person.calculate_fitness()
    #     person.improve_traits_from(other_person=other_person)
    #     self.assertListEqual([1, 1, 1, 1], person.traits)
    #     self.assertEqual(4, person.fitness)

    def test_person_should_improve_traits_from_higher_fitness(self):
        person = Person(traits=[1, 2, 3, 4])
        person.calculate_fitness()
        other_person = Person(traits=[1, 1, 1, 1])
        other_person.calculate_fitness()
        person.improve_traits_from(other_person=other_person)
        self.assertNotEqual([1, 2, 3, 4], person.traits)
        self.assertNotEqual(30, person.fitness)

    def test_person_should_acquire_traits_from_better_person(self):
        person = Person(traits=[1, 2, 3, 4])
        person.calculate_fitness()
        other_person = Person(traits=[1, 1, 1, 1])
        other_person.calculate_fitness()
        person.improve_traits_from(other_person=other_person)
        self.assertNotEqual([1, 2, 3, 4], person.traits)
        self.assertNotEqual(30, person.fitness)

    # def test_person_should_not_acquire_traits_from_worse_person(self):
    #     person = Person(traits=[1, 1, 1, 1], fitness_function=SphereFunction)
    #     person.calculate_fitness()
    #     other_person = Person(traits=[1, 2, 3, 4], fitness_function=SphereFunction)
    #     other_person.calculate_fitness()
    #     best_person = Person(traits=[0, 0, 0, 0], fitness_function=SphereFunction)
    #
    #     person.acquire_traits_from(other_person=other_person, best_person=best_person)
    #     self.assertEqual([1, 1, 1, 1], person.traits)
    #     self.assertEqual(4, person.fitness)

    # Population
    def test_population(self):
        population = PopulationGroup()
        population.populate(person_count=2, traits_count=2, round_off_traits=True, traits_range_min=-10, traits_range_max=10)
        self.assertEqual(2, len(population.population))
        self.assertEqual(2, len(population.population[0].traits))
        self.assertLess(-10, population.population[0].traits[0])
        self.assertGreater(10, population.population[0].traits[0])

    def test_find_best_person(self):
        population = PopulationGroup()
        population.populate(person_count=3, traits_count=4, traits_range_min=-10, round_off_traits=True,
                            traits_range_max=10)
        population.population.extend([
            Person(traits=[0, 0, 0, 0]),
            Person(traits=[1, 1, 1, 1]),
            Person(traits=[1, 2, 3, 4]),
        ])

        population.calculate_population_fitness()
        population.find_best_person()
        self.assertListEqual([0, 0, 0, 0], population.best_person.traits)
